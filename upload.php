<?php  

header('Content-type:application/json; charset=utf-8');

$fileZip = $_FILES['file'];//获取上传文件

echo uploadZip($fileZip);


function uploadZip($files, $path = "./file",$imagesExt=['zip','jpeg','png','jpg','rar']){
    // 判断错误号
    if (@$files['error'] == 00) {
        // 判断文件类型
        $ext = strtolower(pathinfo(@$files['name'],PATHINFO_EXTENSION));
        if (!in_array($ext,$imagesExt)){
            $arr['code'] = 400;
            $arr['msg'] = 'error';
            return json_encode($arr);
        }
        // 判断是否存在上传到的目录
        if (!is_dir($path)){
            mkdir($path,0777,true);
        }
        // 生成唯一的文件名
        $fileName = sha1($files['name']).'.'.$ext;
        //$fileName = md5(uniqid(microtime(true),true)).'.'.$ext;
        // 将文件名拼接到指定的目录下
        $destName = $path."/".$fileName;
        // 进行文件移动
        if (!move_uploaded_file($files['tmp_name'],$destName)){
            $arr['code'] = 401;
            $arr['msg'] = '上传失败';
            return json_encode($arr);
        }
        $arr['code'] = 0;
        $arr['url'] = 'https://rz.if21.cn/file/'.$fileName;
        $arr['msg'] = '上传成功';
        return json_encode($arr);
    }
}